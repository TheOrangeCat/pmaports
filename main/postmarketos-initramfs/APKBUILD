# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=postmarketos-initramfs
pkgver=3.1.0
pkgrel=2
pkgdesc="Base files for the postmarketOS initramfs / initramfs-extra"
url="https://postmarketos.org"
options="!check"  # no tests
provides="postmarketos-ramdisk=$pkgver-r$pkgrel"
provider_priority=10
depends="
	blkid
	btrfs-progs
	busybox-extras
	bzip2
	cryptsetup
	device-mapper
	devicepkg-utils>=0.2.0
	e2fsprogs
	e2fsprogs-extra
	f2fs-tools
	iskey
	kmod
	libinput-libs
	lz4
	multipath-tools
	parted
	postmarketos-fde-unlocker
	postmarketos-mkinitfs>=2.2
	udev
	unudhcpd
	util-linux-misc
	xz
	"
source="
	00-default.modules
	00-initramfs-base.dirs
	00-initramfs-base.files
	00-initramfs-extra-base.files
	init.sh
	init_functions.sh
	unudhcpd.conf
	"
arch="noarch"
license="GPL-2.0-or-later"

build() {
	mkdir -p "$builddir"

	# Replace <<INITRAMFS_PKG_VERSION>> with the actual version
	sed "s|<<INITRAMFS_PKG_VERSION>>|$pkgver-r$pkgrel|" \
		"$srcdir/init.sh" > "$builddir/init.sh"
}

package() {
	install -Dm644 "$srcdir/unudhcpd.conf" \
		"$pkgdir/etc/unudhcpd.conf"

	install -Dm644 "$srcdir/init_functions.sh" \
		"$pkgdir/usr/share/initramfs/init_functions.sh"

	install -Dm755 "$builddir/init.sh" \
		"$pkgdir/usr/share/initramfs/init.sh"

	install -Dm644 "$srcdir"/00-initramfs-base.dirs \
		-t "$pkgdir"/usr/share/mkinitfs/dirs/
	mkdir -p "$pkgdir"/etc/mkinitfs/dirs

	install -Dm644 "$srcdir"/00-default.modules \
		-t "$pkgdir"/usr/share/mkinitfs/modules/
	mkdir -p "$pkgdir"/etc/mkinitfs/modules

	install -Dm644 "$srcdir"/00-initramfs-base.files \
		-t "$pkgdir"/usr/share/mkinitfs/files/
	mkdir -p "$pkgdir"/etc/mkinitfs/files

	install -Dm644 "$srcdir"/00-initramfs-extra-base.files \
		-t "$pkgdir"/usr/share/mkinitfs/files-extra/
	mkdir -p "$pkgdir"/etc/mkinitfs/files-extra

	mkdir -p "$pkgdir"/usr/share/mkinitfs/hooks
	mkdir -p "$pkgdir"/usr/share/mkinitfs/hooks-extra
	mkdir -p "$pkgdir"/etc/mkinitfs/hooks
	mkdir -p "$pkgdir"/etc/mkinitfs/hooks-extra
}

sha512sums="
59be0649ed87a72d93624bd8a2e3f8c99a0f32f7b7a26f99436de782beba55671472c269eeee86440efc87e0d7148a0bb335fa537791092e73878ca21330544a  00-default.modules
9c0e8f6f61d5da191e03a1aa9d5d0ceb5baf1eae6dbb9bfb0af59817783525119ac8394b135f303f7b6434a3eab0b49185fb90379e06823db847a4999c75ce33  00-initramfs-base.dirs
1c64d2da2013213467dfaa40e984e3a7c7e8a885d3327fe164f1e1c091eb9c9f2dfadcc7e1e1ef8a0e7fa36513b2216b4bde97d58c2528a637821a31800ccd34  00-initramfs-base.files
8a4adad3785af474b36a09a05f6a3b2c4b4f43aac331a53b903abfa51ea12be1e3d1d807b7a6e66a1346815f3b0044daf8cd62e21e2dc75d2db13ee265a72985  00-initramfs-extra-base.files
749865c52a229569c0cd346dd97fe10d49361124eba95bd0dbb25d0737d1efc139c8e2e31f4b7654747ffecfa77403886f1e1fe7d548d47aee03ced43d8ef2eb  init.sh
1d3337ace7611853579945dc6563556ced66f386a7f906835425bac9e45c7734682b85b381656dfd5d3b0a727a39e5718e77f0d19af42562c199df25ea9fda59  init_functions.sh
ba3275a9af788c7c782322a22a0f144d5e50e3498ea6886486a29331f23ae89cd32d500a3635cfa7cab369afba92edc18aeca64ccbf0cd589061cce23d15b46c  unudhcpd.conf
"

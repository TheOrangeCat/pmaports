# Maintainer: Caleb Connolly <caleb@postmarketos.org>
# Kernel config based on: arch/arm64/configs/defconfig

pkgname=linux-next
pkgver=6.10_git20240620
pkgrel=2
_nextver="${pkgver#*_git}"
pkgdesc="Linux next kernel"
arch="aarch64"
_carch="arm64"
_flavor="generic-trailblazer"
url="https://kernel.org"
license="GPL-2.0-only"
# FIXME: kconfigcheck isn't supported since we use fragments
options="!strip !check !tracedeps pmb:cross-native"
source="
	https://gitlab.com/linux-kernel/linux-next/-/archive/next-$_nextver/linux-next-next-$_nextver.tar.gz
	devices.config
	pmos.config
"
makedepends="
	bash
	bc
	bison
	devicepkg-dev
	findutils
	flex
	openssl-dev
	perl
	zstd
	git
	python3
"
builddir="$srcdir/linux-next-next-$_nextver"

prepare() {
	default_prepare
	cp "$srcdir/pmos.config" "$builddir"/arch/"$_carch"/configs/
	cp "$srcdir/devices.config" "$builddir"/arch/"$_carch"/configs/

	# Generate .config
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-next-$_nextver" \
		defconfig pmos.config devices.config
}

build() {
	unset LDFLAGS
	make O="$_outdir" ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	install -Dm644 "$builddir/arch/$_carch/boot/vmlinuz.efi" \
			"$pkgdir/boot/linux.efi"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs

	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/next-"$_nextver"/kernel.release
}

sha512sums="
2b3520006954472501020034ddb6040e2df83d20a89864b780adb8a1dcae9768bd7c810d9d81fb917fefda256ebbf0c54d949214b9c5e12b3787c090ba7db345  linux-next-next-20240620.tar.gz
e5b8530b8410d41d60c6c3a5cb4f73cdcdbcf432a8453ddb0b8901bf20083df4ae8c44ff2771aaa17c4a78afddfd604f929f05839bb513ad60809fbd7adb7056  devices.config
d52f692935675b5b9300c7983848a26149914687a8b3aef3b90f50da3f6534de9b490c6778d490bb5a4202f1a45cc4d9178a1e1b8270a8d612d4181721629c02  pmos.config
"
